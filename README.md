# saucedemo

Test login of Swag Labs Saucedemo

Tiny example of using Playwright, Typescript and Cucumber to test log in to Swag Labs Saucedemo website.

Ensure that NodeJS and NPM are installed on your system. Clone this repo and run:

```npm install```

The tests can be run using:

```npm test```